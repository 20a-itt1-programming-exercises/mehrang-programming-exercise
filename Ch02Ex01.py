#   Chapter: 02 Page:23
#   Exercise 1: Type the following statements in the Python interpreter to
#   see what they do:
#   5
#   x = 5
#   x + 1


5
x = 5
x + 1   #   if we print it, the answer will be 6