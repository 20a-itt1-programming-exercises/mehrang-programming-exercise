#   Chapter: 02 Page:30

#   Exercise 2: Write a program that uses input to prompt a user for their
# name and then welcomes them.
# Enter your name: Chuck
# Hello Chuck

print("\n******Chapter:02 Page:30 Exercise:02******")
name = input("What is your name? \n")
print("Hello " , name , " :-)")