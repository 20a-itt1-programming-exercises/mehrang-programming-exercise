#   Chapter: 02 Page:30

#   Exercise 5: Write a program which prompts the user for a Celsius tem-
# perature, convert the temperature to Fahrenheit, and print out the
# converted temperature.

print("\n******Chapter:02 Page:30 Exercise:05******")
celTemp=float(input("What is the temp in C° ?   "))
fahrTemp=float((celTemp * 9.0 / 5.0)+32)
print(" in Fahrenheit it is: ", fahrTemp)