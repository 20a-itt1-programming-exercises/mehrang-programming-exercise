#   Chapter: 03 Page: 40

#   Exercise 1: Rewrite your pay computation to give the employee 1.5
#   times the hourly rate for hours worked above 40 hours.
#   Enter Hours: 45
#   Enter Rate: 10
#   Pay: 475.0
print("\n******Chapter:03 Page:40 Exercise:01******")
hours = float(input('Enter Hours? '))
rate = float(input('Enter Rate=? '))
if hours > 40:
    pay = (40 * rate) + ((hours - 40) * (rate * 1.5))
else:
    pay = (hours * rate)
print("You will get: ", pay)