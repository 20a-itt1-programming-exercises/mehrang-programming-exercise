#   Chapter: 03 Page: 40

#   Exercise 2: Rewrite your pay program using try and except so that your
# program handles non-numeric input gracefully by printing a message
# and exiting the program. The following shows two executions of the
# program:
# Enter Hours: 20
# Enter Rate: nine
# Error, please enter numeric input
# Enter Hours: forty
# Error, please enter numeric input

print("\n******Chapter:03 Page:40 Exercise:02******")
try:
    hours = float(input('Enter Hours: '))
    rate = float(input('Enter Rate: '))

    if hours > 40:
       pay = (40 * rate) + ((hours - 40) * (rate * 1.5))
    else:
       pay = (hours * rate)
    print("You will get: ", pay)
except:
    print("Error, please enter numeric input")