#   Chapter: 03 Page: 40

#   Exercise 3: Write a program to prompt for a score between 0.0 and
# 1.0. If the score is out of range, print an error message. If the score is
# between 0.0 and 1.0, print a grade using the following table:
# Score Grade
# >= 0.9 A
# >= 0.8 B
# >= 0.7 C
# >= 0.6 D
# < 0.6 F
# Enter score: 0.95
# A
# Enter score: perfect
# Bad score
# Enter score: 10.0
# Bad score
# Enter score: 0.75
# C
# Enter score: 0.5
# F
# Run the program repeatedly as shown above to test the various different values for
# input.

print("\n******Chapter:03 Page:40 Exercise:03******")
try:
    score = float(input("Enter score:\n"))
    if score > 1.0:
        print(" Enter a number between 0.0 to 1.0\n")
    elif 0.9 <= score <= 1.0:
        print("A")
    elif 0.8 <= score <= 1.0:
        print("B")
    elif 0.7 <= score <= 1.0:
        print("C")
    elif 0.6 <= score <= 1.0:
        print("D")
    elif 0.6 > score <= 1.0:
        print("F")
except:
    print("The number is not between 0.0 and 1.0")