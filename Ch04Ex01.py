#   Chapter: 04 Page: 46


#   Exercise 1: Run the program on your system and see what numbers
# you get. Run the program more than once and see what numbers you
# get.


print("\n******Chapter:04 Page:46 Exercise:01******")
import random
for i in range(5):
    x = random.random()
    print("random: ",x)
    y = random.randint(5,10)
    print("randint:", y)