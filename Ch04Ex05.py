#   Chapter: 04 Page: 54


#   Exercise 5: What will the following Python program print out?
# def fred():
#   print("Zap")
# def jane():
#   print("ABC")
# jane()
# fred()
# jane()
# a) Zap ABC jane fred jane
# b) Zap ABC Zap
# c) ABC Zap jane
# d) ABC Zap ABC
# e) Zap Zap Zap


print("\n******Chapter:04 Page:54 Exercise:05******")

print("d")