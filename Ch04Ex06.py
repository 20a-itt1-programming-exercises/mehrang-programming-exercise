#   Chapter: 04 Page: 54


#   Exercise 6: Rewrite your pay computation with time-and-a-half for over-
# time and create a function called computepay which takes two parameters
# (hours and rate).
# Enter Hours: 45
# Enter Rate: 10
# Pay: 475.0


print("\n******Chapter:04 Page:54 Exercise:06******")

def computepay(hours, rate):
    if hours > 40:
        pay = float((40 * rate) + ((hours - 40) * (rate * 1.5)))
        return pay
    else:
        pay = float(rate * hours)
        return pay

try:
    hours = float(input("Enter Hours\n"))
    rate = float(input("Enter Rate?\n"))
    pay = computepay(hours, rate)
    print("Your payment for ", hours, "Hours is: ", pay, " worth of nothing.")
except:
    print("Please enter a number")
