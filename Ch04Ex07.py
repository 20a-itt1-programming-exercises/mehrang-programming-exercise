#   Chapter: 04 Page: 54


#   Exercise 7: Rewrite the grade program from the previous chapter using
# a function called computegrade that takes a score as its parameter and
# returns a grade as a string.
# Score Grade
# >= 0.9 A
# >= 0.8 B
# >= 0.7 C
# >= 0.6 D
# < 0.6 F
# Enter score: 0.95
# A
# Enter score: perfect
# Bad score
# Enter score: 10.0
# Bad score
# Enter score: 0.75
# C
# Enter score: 0.5
# F
# Run the program repeatedly to test the various different values for input.


print("\n******Chapter:04 Page:54 Exercise:07******")

def computegrade(score):
        if score > 1.0:
            print(" Enter a number between 0 to 1.0\n")
        elif 0.9 <= score <= 1.0:
            print("A")
        elif 0.8 <= score <= 1.0:
            print("B")
        elif 0.7 <= score <= 1.0:
            print("C")
        elif 0.6 <= score <= 1.0:
            print("D")
        elif 0.6 > score <= 1.0:
            print("F")

        return score

try:
    score = float(input("Enter a number between 0.0 to 1.0:\n"))
    grade = str(computegrade(score))
except:
    print("The number is not between 0 and 1.0")
