1.14 Exercises
Exercise 1: What is the function of the secondary memory in a com-
puter?
a) Execute all of the computation and logic of the program
b) Retrieve web pages over the Internet
c) Store information for the long term, even beyond a power cycle
d) Take input from the user
Answer: C

Exercise 2: What is a program?
Answer: set of instructions that specifies a computation.

Exercise 3: What is the difference between a compiler and an inter-
preter?
Answer: interpreter Executes a program but compiler prepares the written program for executing.

Exercise 4: Which of the following contains “machine code”?
a) The Python interpreter
b) The keyboard
c) Python source file
d) A word processing document
Answer: A

Exercise 5: What is wrong with the following code:
1.14. EXERCISES 17
>>> primt 'Hello world!'
File "<stdin>", line 1
primt 'Hello world!'
^
SyntaxError: invalid syntax
>>>
Answer: it should be like: print('Hello world!')

Exercise 6: Where in the computer is a variable such as “x” stored after
the following Python line finishes?
x = 123
a) Central processing unit
b) Main Memory
c) Secondary Memory
d) Input Devices
e) Output Devices
Answer: b

Exercise 7: What will the following program print out:
x = 43
x = x + 1
print(x)
a) 43
b) 44
c) x + 1
d) Error because x = x + 1 is not possible mathematically
Answer: b

Exercise 8: Explain each of the following using an example of a hu-
man capability:
(1) Central processing unit
Answer: Braain

(2) Main Memory
Answer:

(3) Secondary Memory
Answer:

(4) Input Device, and
Answer:

(5) Output Device. For example, “What is the human equivalent to a Central Processing Unit”?
Answer:

Exercise 9: How do you fix a “Syntax Error”?
Answer: check the Python's grammer rules